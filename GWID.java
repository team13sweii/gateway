package javaapplication4;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;

/*
@author ZeeYo /

public class JavaApplication4 {

// start new thread
static class MyTask implements Runnable
{
boolean running = false;
public void start() {
running = true;
new Thread(this).start();
}

public void run() {

while(running) {

    System.out.println("\ntesting\n");

     try {
        Thread.sleep(1000); // you were doing thread.sleep()! to sleep for 1000 msec


     //initialize the socket on local host with 6191 socket to read data from dynagnostics            
    Socket clientSocket = null;
    try {
        clientSocket = new Socket("127.0.0.1", 6191); 
    } catch (IOException ex) {
        Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex); 
    }
    // GetInputStream from socket
InputStream is = null;
   try {
       is = clientSocket.getInputStream();
   } catch (IOException ex) {
       Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
   }
   // Get Output stream from socket
PrintWriter pw = null;
   try {
       pw = new PrintWriter(clientSocket.getOutputStream());
   } catch (IOException ex) {
       Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
   }
   //Create packet to send to the website
pw.println("GET / HTTP/1.0");
pw.println();
pw.flush();
byte[] buffer = new byte[1024];
int read;
   try {
       read = is.read(buffer);
           String output = new String(buffer, 0, read);
           System.out.print(output);

           String only_id= output.substring(output.indexOf(":")+1,output.indexOf(",")); // Get value of only id from string.
           String only_time=output.substring(output.lastIndexOf(":") -5); // Get value of LastIndex from string.
           System.out.print("\n"+only_id+" "+only_time+"\n");
           System.out.flush();
           // Send data to the server URL
         String url = ("https://dreamteam13.softwareengineeringii.com/diag?ID="+only_id+"&timestamp="+only_time);
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setConnectTimeout(3400);

            con.setRequestProperty("Content-Type", "application/json; charset-UTF-8");
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.close();
            con.disconnect();


       //}  
     } catch (IOException ex) {
       Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
   }
   
   ;
try {
clientSocket.close();
} catch (IOException ex) {
Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
}

}
  catch (InterruptedException e) {
        e.printStackTrace();
    }

}

}
}

public static void main(String[] args) {

    //start the task

    new MyTask().start();

}

}